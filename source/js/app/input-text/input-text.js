(function () {
    'use strict';

    var rfsInputText = new InputText(
        [
            {
                'attribute' : 'ng-click',
                'value' : 'reloadFrame()',
                'ref' : 'input'
            },
            {
                'attribute' : 'ng-model',
                'value' : 'textInput',
                'ref' : 'input'
            },
        ],
        [   {
                'element' : document.createElement('ng-transclude'),
                'ref' : 'pre-label'
            }
        ]
    );

    angular
        .module('testApp')
        .directive('rfsInputText', inputText);

    function inputText() {

        var directive = {
            bindToController: true,
            controller: InputTextController,
            controllerAs: 'vm',
            link: link,
            restrict: 'AE',
            scope: {},
            transclude: true,
            template: rfsInputText.getElement().outerHTML
            //templateUrl: 'input-text.html'
        };
        return directive;

        function link(scope, element, attrs) {

            scope.textInput = 'hallo';

            scope.$watch('textInput', function(newVal){
                console.log('change InputText-component', newVal);
            })

            scope.reloadFrame = function() {
                console.log('reload frame');
            }
        }
    }

    function InputTextController() {

    }

})();