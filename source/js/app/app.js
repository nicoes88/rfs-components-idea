(function () {
    'use strict';

    var rfsInputText = new InputText(
        [
            {
                'attribute' : 'ng-click',
                'value' : 'reloadFrame()',
                'ref' : 'input'
            },
            {
                'attribute' : 'ng-model',
                'value' : 'textInput',
                'ref' : 'input'
            },
        ],
        [   {
                'element' : document.createElement('ng-transclude'),
                'ref' : 'pre-label'
            }
        ]
    );

    angular
        .module('testApp', [])
        .run(function($templateCache) {
        $templateCache.put('input-text.html', rfsInputText.getElement().outerHTML);
    });

})();