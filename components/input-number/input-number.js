//  InputNumberComponent
function InputNumber(attributes, insertions){

    var el,
        inputText = new InputText(attributes, insertions);

    activate(attributes, insertions);

    return {
        getElement : getElement
    }

    //////

    function activate(attributes, insertions) {

        createElement();

        if(attributes){
            attachAttributes(attributes);
        }
        if(insertions){
            insertElements(insertions);
        }
    }
    
    function getElement(){
        return el;
    }

    function createElement(){
        el = inputText.getElement();
        var input = el.querySelector('[data-ref="input"]');
        input.type = 'number';
        input.setAttribute('min', 2);
        input.setAttribute('max', 100);
        input.setAttribute('value', 1);
    }

    function attachAttributes(attributes){
        for(var i = 0; i < attributes.length; i++){
            var targetEl = el.querySelector('[data-ref="'+ attributes[i].ref +'"]');
            targetEl.setAttribute(attributes[i].attribute, attributes[i].value);
        }
    }
    
    function insertElements(attributes){
        for(var i = 0; i < insertions.length; i++){
            var targetEl = el.querySelector('[data-ref="'+ insertions[i].ref +'"]');
            targetEl.appendChild(insertions[i].element);
        }
    }
};