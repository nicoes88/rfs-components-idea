//  InputTextComponent
//
//  Attributes:
//  { label : value }
function InputText(attributes, insertions){

    var el;

    activate(attributes, insertions);

    return {
        getElement : getElement
    }

    //////

    function activate(attributes, insertions) {

        createElement();

        if(attributes){
            attachAttributes(attributes);
        }
        if(insertions){
            insertElements(insertions);
        }
    }


    function getElement(){
        return el;
    }

    function createElement(){

        var br = document.createElement('br');

        el = document.createElement('div');
        el.className = "input-text input-text--big";

        el.appendChild(createPrelabel());
        el.appendChild(br);
        el.appendChild(createLabel());
        el.appendChild(br);
        el.appendChild(createInput());
    }

    function attachAttributes(attributes){
        for(var i = 0; i < attributes.length; i++){
            var targetEl = el.querySelector('[data-ref="'+ attributes[i].ref +'"]');
            targetEl.setAttribute(attributes[i].attribute, attributes[i].value);
        }
    }
    
    function insertElements(attributes){
        for(var i = 0; i < insertions.length; i++){
            var targetEl = el.querySelector('[data-ref="'+ insertions[i].ref +'"]');
            targetEl.appendChild(insertions[i].element);
        }
    }

    function createPrelabel(){
        var prelabel = document.createElement('em');
        var prelabelText = document.createTextNode('Belangrijk!');
        prelabel.setAttribute('data-ref', 'pre-label');
        prelabel.appendChild(prelabelText);

        return prelabel;
    }

    function createLabel(){
        var label  = document.createElement('label');
        var labelText = document.createTextNode('Geef een waarde');
        label.appendChild(labelText);

        return label;
    }

    function createInput(){
        var input = document.createElement('input');
        input.type = 'text';
        input.className = "input-text__input";
        input.setAttribute('data-ref', 'input');

        return input;
    }
};